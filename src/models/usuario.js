const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const usuarioSchema = mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});

usuarioSchema.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

usuarioSchema.methods.comparePassword = async (password, user_password) => {
    return await bcrypt.compare(password, user_password);
}

usuarioSchema.pre("save", async function (next) {
    const usuario = this;

    //validamos que el usuario existe
    if (!usuario.isModified("password")) return next();

    usuario.password = await usuario.encryptPassword(usuario.password);
    next();
})

module.exports = mongoose.model('Usuario', usuarioSchema);