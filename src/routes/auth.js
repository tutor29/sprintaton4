const { Router } = require('express');
const { registro, login } = require('../controllers/auth.controller');

const router = Router();

router.post('/registrar', registro);
router.post('/login', login);

module.exports = router;

