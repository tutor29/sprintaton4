//incluimos el modulo express
const express = require("express");

const tareaSchema = require("../models/tarea");

//creamos el router
const router = express.Router();

//definir las rutas

//crear tareas
router.post("/tareas", (req, res) => {
  const tarea = tareaSchema(req.body);
  tarea
    .save()
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
});

//obtener todas las tareas
router.get("/tareas", (req, res) => {
  tareaSchema
    .find()
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
});

//obtener una tarea
router.get("/tareas/:id", (req, res) => {
  const { id } = req.params;
  tareaSchema
    .findById(id)
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
});

//actualizar una tarea
router.put("/tareas/:id", (req, res) => {
  const { id } = req.params;
  const { titulo, estado, descripcion } = req.body;
  tareaSchema
    .updateOne({ _id: id }, { $set: { titulo, estado, descripcion } })
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
});

//eliminar una tarea
router.delete("/tareas/:id", (req, res) => {
    const { id } = req.params;
    tareaSchema
      .remove({ _id: id })
      .then((data) => res.json(data))
      .catch((error) => res.json({ mensaje: error }));
  });

module.exports = router;
