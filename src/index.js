//incluimos el modulo express
const express = require("express");

//incluimos el modulo moogose
const mongoose = require("mongoose");

//incluimos para usar variables de ambiente
require("dotenv").config();

//importar cors
const cors = require("cors");

//importar las rutas
const tareasRutas = require("./routes/tareas");
const authRutas = require('./routes/auth');
//ejecutar express
const app = express();
//habilitar cors
app.use(cors());

//middleware
app.use(express.json());
app.use('/api', tareasRutas);
app.use('/api', authRutas);

//routes
app.get('/', (req, res) =>{
    res.send('Bienvenido al api')
})

// mongodb conexion
mongoose.connect(process.env.MONGODB_URI)
.then(() => console.log("Conectado a MongoDB Atlas."))
.catch((error) => console.error(error));

//usar el objeto para que escuche en el puerto
const port = process.env.PORT || 4000;
app.listen(port, () => console.log('Servidor en el puerto', port));