const Usuario = require('../models/usuario');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

const verificarToken = async (req, res, next) => {
    const token = req.headers["x-access-token"];

    if (!token) return res.status(403).json({ status: "403"});

    try {
        const decofique = jwt.verify(token, process.env.SECRECT);
        req.usuarioId = decoded.id;

        const usuario = await Usuario.findById(req.usuarioId);

        if(!usuario) return res.status(404).json({ status: "404" })

        return next();
    } catch (error) {
        return res.status(500).json({ status: "500" });
    }
}

module.exports = verificarToken;