const Usuario = require('../models/usuario')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();

const registro = async (req, res) => {
    try {
        
        const email = await Usuario.findOne({ email: req.body.email })
        if(email) return res.status(405).json({ message: "No se puede registrar con el correo ", status: "405"});

        const username = await Usuario.findOne({ username: req.body.username })
        if ( username ) return res.status(405).json({ message: "El usarname ya se encuentra registrado", status: "405"});

        const usuario = await Usuario(req.body).save();

        const token = jwt.sign(
            {id: usuario._id},
            process.env.SECRECT,
            {
                expiresIn: 86400, // 24 horas
            }
        )

        return res.status(200).json({ token: token, status: "200"})
    } catch (error) {
        console.log(error.message)
        return res.status(500).json({ status: "500"})
    }
};

const login = async (req, res) => {
    try {
        const usuario = await Usuario.findOne({ email: req.body.email });

        if(!usuario) return res.status(404).json({ status: "404"});
        
        const passswordValidada = await bcrypt.compare(req.body.password, usuario.password);

        if(!passswordValidada) return res.status(401).json({ status: "401"});

        const token = jwt.sign(
            {id: usuario._id},
            process.env.SECRECT,
            {
                expiresIn: 86400, // 24 horas
            }
        )
        return res.status(200).json({ token: token, status: "200"})
    } catch (error) {
        console.log(error.message);
        return res.status(500).json({ status: "500"})
    }
};

module.exports = {
    registro,
    login,
}